CHANGELOG for 1.0.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.0 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.0.5 (2016-08-10)
    * Changed dev-master alias to 1.0-dev

* 1.0.4 (2016-08-10)
    * Moved source code to ``src``
    * Removed ``composer.lock``
    * Changed ``zerustech/io`` version to ``^1.1.0``
    * Changed ``zerustech/threaded`` version to ``^1.1.0``

* 1.0.3 (2016-07-09)
    * Fixed comment and cs issues.

* 1.0.2 (2016-07-09)
    * Added comopser.lock

* 1.0.1 (2016-07-09)
    * Removed composer.lock.
    * Fixed FQN issues.
    * Removed directory separator
    * Fixed ``__DIR__`` issue.

* 1.0.0 (2016-07-08)
    * First release of zerustech/terminal.
    * Added ``Terminfo\Parser``
    * Added ``Terminfo\Terminfo``
    * Added ``Tool\CursorTool``
    * Added ``Tool\ScreenTool``
    * Added ``Terminal``
