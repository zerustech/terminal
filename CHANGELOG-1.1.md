CHANGELOG for 1.1.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.1 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.1.1 ()
    * Changed zerustech/io version to ``>=2.1.0-dev`` 
    * Changed zerustech/threaded version to ``^1.1.0-dev``

* 1.1.0 (2016-08-21)
    * Changed code for zerustech/io: 2.0.0
